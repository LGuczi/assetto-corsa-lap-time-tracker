SELECT 'DROP FUNCTION ' || oid::regprocedure
FROM   pg_proc
WHERE  proname = 'select_filteredlaps'
AND    pg_function_is_visible(oid);
