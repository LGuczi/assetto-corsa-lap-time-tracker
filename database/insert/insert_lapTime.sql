CREATE OR REPLACE PROCEDURE Insert_LapTime
(
    driver VARCHAR(50),
    carId VARCHAR(50),
    carName VARCHAR(50),
    carClass VARCHAR(50),
    carType VARCHAR(50),
    trackId VARCHAR(50),
    trackName VARCHAR(50),
    lapTime INT,
    validLap BOOLEAN,
    dateSet TIMESTAMP
)
LANGUAGE plpgsql
AS $$
    BEGIN
        IF NOT EXISTS (SELECT Id FROM Cars WHERE Id = carId) THEN
            INSERT INTO Cars (Id, Pretty_Name, Car_Class, Vehicle_Type)
            VALUES(carId, carName, carClass, carType);
        END IF;

        IF NOT EXISTS (SELECT Id FROM Tracks WHERE Id = trackId) THEN
            INSERT INTO Tracks (Id, Pretty_Name)
            VALUES(trackId, trackName);
        END IF;

        INSERT INTO Lap_Times (Driver_Name, Car_Id, Track_Id, Lap_Time, Lap_Valid, Date_Set)
        VALUES (driver, carId, trackId, lapTime, validLap, dateSet);
    END
$$;
