CREATE OR REPLACE FUNCTION select_filteredLaps
(
    driverName VARCHAR(50) DEFAULT NULL,
    carName VARCHAR(50) DEFAULT NULL,
    trackName VARCHAR(50) DEFAULT NULL,
    className VARCHAR(50) DEFAULT NULL,
    dateStart TIMESTAMP DEFAULT NULL,
    dateEnd TIMESTAMP DEFAULT NULL,
    onlyValidLaps BOOLEAN DEFAULT False,
    onlyBestLaps BOOLEAN DEFAULT False
)
RETURNS TABLE (
    id BIGINT,
    driver VARCHAR,
    car VARCHAR,
    carClass VARCHAR,
    carType VARCHAR,
    track VARCHAR,
    lap INT,
    validLap BOOLEAN,
    dateTimeSet TIMESTAMP
) 
AS $$
BEGIN
    IF onlyBestLaps IS NULL OR onlyBestLaps = False 
    THEN
        RETURN QUERY SELECT
            lap_times.id,
            lap_times.driver_name,
            cars.pretty_name,
            cars.car_class,
            cars.vehicle_type,
            tracks.pretty_name,
            lap_times.lap_time,
            lap_times.lap_valid,
            lap_times.date_set
        FROM
            lap_times
            INNER JOIN cars ON lap_times.car_id = cars.id
            INNER JOIN tracks ON lap_times.track_id = tracks.Id
        WHERE
            (driverName IS NULL OR LOWER(lap_times.driver_name) LIKE '%' || LOWER(driverName) || '%')
            AND (carName IS NULL OR LOWER(cars.pretty_name) LIKE '%' || LOWER(carName) || '%')
            AND (trackName IS NULL OR LOWER(tracks.pretty_name) LIKE '%' || LOWER(trackName) || '%')
            AND (className IS NULL OR LOWER(cars.car_class) LIKE '%' || LOWER(className) || '%')
            AND (lap_times.date_set >= dateStart AND lap_times.date_set < dateEnd)
            AND ((onlyValidLaps IS NULL OR onlyValidLaps <> true) OR lap_times.lap_valid = true)
        ORDER BY
            lap_times.lap_time ASC;
    ELSE
        RETURN QUERY SELECT
            bestLaps.id,
            bestLaps.driver_name,
            cars.pretty_name,
            cars.car_class,
            cars.vehicle_type,
            tracks.pretty_name,
            bestLaps.lap_time,
            bestLaps.lap_valid,
            bestLaps.date_set
        FROM
            (
                SELECT
                    ROW_NUMBER() OVER (PARTITION BY driver_name, car_id, track_id ORDER BY lap_time ASC) AS rowNumber,
                    lap_times.id,
                    driver_name,
                    car_id,
                    track_id,
                    lap_time,
                    lap_valid,
                    date_set
                FROM
                    lap_times
            ) AS bestLaps
            INNER JOIN cars ON bestLaps.car_id = cars.id
            INNER JOIN tracks ON bestLaps.track_id = tracks.Id
        WHERE
            (driverName IS NULL OR LOWER(bestLaps.driver_name) LIKE '%' || LOWER(driverName) || '%')
            AND (carName IS NULL OR LOWER(cars.pretty_name) LIKE '%' || LOWER(carName) || '%')
            AND (trackName IS NULL OR LOWER(tracks.pretty_name) LIKE '%' || LOWER(trackName) || '%')
            AND (className IS NULL OR LOWER(cars.car_class) LIKE '%' || LOWER(className) || '%')
            AND (bestLaps.date_set >= dateStart AND bestLaps.date_set < dateEnd)
            AND ((onlyValidLaps IS NULL OR onlyValidLaps <> true) OR bestLaps.lap_valid = true)
            AND bestLaps.rowNumber = 1
        ORDER BY
            bestLaps.lap_time ASC;
    END IF;
END;
$$
LANGUAGE plpgsql