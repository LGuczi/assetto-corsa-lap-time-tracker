CREATE TABLE Cars (
    Id VARCHAR(64), 
    Pretty_Name VARCHAR(64), 
    Car_Class VARCHAR(32), 
    Vehicle_Type VARCHAR(64),
    CONSTRAINT PK_CARS_ID PRIMARY KEY (Id)
);