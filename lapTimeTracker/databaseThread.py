import ac
from threading import Thread

import urllib3
import jwt

class DBThread (Thread):
    BASE_URL = "http://99.250.30.224:3000/"
    API_POST_LAP = "addLap"
    CLIENT_NAME = "consumer-assetto-corsa"
    SECRET = "derekIsACoolDude"

    def __init__(self, threadID, name, sessionData):
        Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.sessionData = sessionData

    def run(self):
        self.addLapToDatabase(self.sessionData)

    def addLapToDatabase(self, sessionData):
        apiCall = self.BASE_URL + self.API_POST_LAP
        http = urllib3.PoolManager(timeout=10.0)
        encodedJwt = jwt.encode(sessionData, self.SECRET, algorithm='HS256')
        try:
            response = http.request('POST', apiCall, headers={'jwt-token': encodedJwt, 'jwt-consumer': self.CLIENT_NAME})
            status = response.status
            ac.log("Response: " + str(status))
        except Exception as e:
            ac.log(str(e))