import os
import sys
import platform
import ac
import acsys
from session import Session

PATH_APP = os.path.dirname(__file__)
path_stdlib=''
if platform.architecture()[0] == '64bit':
    path_stdlib = os.path.join(PATH_APP, 'stdlib64')
else:
    path_stdlib = os.path.join(PATH_APP, 'stdlib')
sys.path.insert(0, path_stdlib)
sys.path.insert(0, os.path.join(PATH_APP, 'third_party'))
os.environ['PATH'] = os.environ['PATH'] + ';.'

from databaseThread import DBThread
from sim_info import info

def acMain(ac_version):
    appWindow = ac.newApp("Lap Time Tracker")
    ac.setSize(appWindow, 200, 200)
    ac.log("Starting lap tracker")

    return "Lap Time Tracker"

#global
lapCount = 0
lapValid = True
def acUpdate(deltaT):
    global lapCount
    global lapValid

    if (lapValid == True and info.physics.numberOfTyresOut == 4):
        lapValid = False

    lapsCompleted = info.graphics.completedLaps
    if (lapsCompleted > lapCount):
        lapCount = lapsCompleted
        lastLap = info.graphics.iLastTime
        if (lastLap == 0 or lastLap == None):
            ac.log("Failed to get lastLap")
        else:
            session = Session(lastLap, lapValid)
            isValidSession, msg = session.validate()
            if (isValidSession == False):
                ac.log(msg)
                return

            sessionData = session.getSessionDataObject()
            t1 = DBThread(1, "Thread - Add to DB", sessionData)
            t1.start()

        lapValid = True
    elif (lapsCompleted < lapCount):
        lapCount = 0
        lapValid = True
    