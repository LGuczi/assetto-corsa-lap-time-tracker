import os
import sys
import platform
import ac
import json
import time
import re

class Session:
    CAR_PATH = "content/cars/"
    TRACK_PATH = "content/tracks/"
    
    def __init__(self, lastLap, lapValid):
        self.driverName = ac.getDriverName(0)

        self.carId = ac.getCarName(0)
        carData = self.getCarData(self.carId)
        if (carData != None):
            self.carName = carData["name"]
            self.carType = carData["class"]
            self.carClass = self.getCarClass(carData["tags"])
        else:
            self.carName = None
            self.carType = None
            self.carClass = None

        trackName = ac.getTrackName(0) 
        trackConfig = ac.getTrackConfiguration(0)
        self.trackId = self.createTrackId(trackName, trackConfig)
        trackData = self.getTrackData(trackName, trackConfig)
        if (trackData != None):
            self.trackPrettyName = trackData["name"]
        else:
            self.trackPrettyName = None

        self.dateTime = time.asctime()
        self.lapTime = lastLap
        self.lapValid = str(lapValid)

    def setLapTime(self, lapTime):
        self.lapTime = lapTime

    def setLapValid(self, lapValid):
        self.lapValid = str(lapValid)

    def getCarData(self, carName):
        carInfoFile = self.CAR_PATH + carName + "/ui/ui_car.json"
        return self.openAndReadJsonFile(carInfoFile)

    def getCarClass(self, tags):
        if (tags is None):
            return None

        for tag in tags:
            if "#" in tag:
                newTag = re.sub("#", "", tag)
                return newTag

        return None

    def getTrackData(self, trackName, trackConfig):
        if (trackConfig == ""):
            trackFileName = self.TRACK_PATH + trackName + "/ui/ui_track.json"
        else:
            trackFileName = self.TRACK_PATH + trackName + "/ui/" + trackConfig + "/ui_track.json"

        return self.openAndReadJsonFile(trackFileName)

    def createTrackId(self, trackName, trackConfig):
        trackId = trackName
        if (trackConfig != None and trackConfig != ""):
            trackId += " - " + trackConfig

        return trackId

    def getSessionDataObject(self):
        return {
            'driver': self.driverName,
            'carId': self.carId,
            'carName': self.carName,
            'carClass': self.carClass,
            'carType': self.carType,
            'trackId': self.trackId,
            'trackName': self.trackPrettyName,
            'lapTime': self.lapTime,
            'lapValid': self.lapValid,
            'dateSet': self.dateTime
        }

    def openAndReadJsonFile(self, filename):
        try:
            with open(filename, "r") as json_file:
                data = json.load(json_file, strict=False)
        except Exception as e:
            ac.log(filename + ": " + str(e))
            data = None

        return data

    def validate(self):
        if (self.driverName is None or self.driverName == ""):
            return False, "Driver name invalid"
        elif (self.carId is None or self.carId == ""):
            return False, "CarId Invalid"
        elif (self.carName is None or self.carName == ""):
            return False, "carName invalid"
        # elif (self.carType is None or self.carType == ""):
            # return False, "carType invalid"
        # elif (self.carClass is None or self.carClass == ""):
            # return False, ""
        elif (self.trackId is None or self.trackId == ""):
            return False, "trackId invalid"
        elif (self.trackPrettyName is None or self.trackPrettyName == ""):
            return False, "trackPrettyName invalid"
        elif (self.dateTime is None or self.dateTime == ""):
            return False, "dateTime invalid"
        elif (self.lapTime is None or self.lapTime == 0):
            return False, "lapTime invalid"
        elif (self.lapValid is None or self.lapValid == "" or (self.lapValid.lower() != "true" and self.lapValid.lower() != "false")):
            return False, "lapValid invalid"

        return True, "OK"
