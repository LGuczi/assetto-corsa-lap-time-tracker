const express = require("express");
const sanitizer = require("express-sanitizer");
const _ = require("lodash");
const jwt = require('jsonwebtoken');
const bodyParser = require("body-parser");

const app = express();

const Pool = require("pg").Pool
const pool = new Pool({
    user: "ac",
    host: "localhost",
    database: "assettocorsa",
    password: "3Lg)GvQ?Rjt:",
    port: 5432,
})

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(sanitizer());

const PORT = 3000;
const APP_CONSUMER_NAME = "consumer-assetto-corsa";
const SECRET = "derekIsACoolDude";

app.listen(PORT, () => {
    console.log(`App running on port ${PORT}.`);
})

const JWTVerifier = async(request, response, next) => {
    const jwtToken = request.headers["jwt-token"];
    const jwtConsumer = request.headers["jwt-consumer"];
    if (jwtToken == "undefined" || jwtConsumer == "undefined") {
        return response.status(400).send();
    }
    if (jwtConsumer !== APP_CONSUMER_NAME) {
        return response.status(403).send();
    }

    try {
        jwt.verify(jwtToken, SECRET);
        const decodedJwt = jwt.decode(jwtToken, { complete: true });
        response.locals.driver = request.sanitize(decodedJwt.payload.driver);

        response.locals.carId = request.sanitize(decodedJwt.payload.carId);
        response.locals.carName = request.sanitize(decodedJwt.payload.carName);
        response.locals.carClass = request.sanitize(decodedJwt.payload.carClass);
        response.locals.carType = request.sanitize(decodedJwt.payload.carType);

        response.locals.trackId = request.sanitize(decodedJwt.payload.trackId);
        response.locals.trackName = request.sanitize(decodedJwt.payload.trackName);

        // For some reason sanitization doesn't work on numbers anymore
	    // response.locals.lapTime = request.sanitize(decodedJwt.payload.lapTime);
        response.locals.lapTime = decodedJwt.payload.lapTime;
        response.locals.lapValid = request.sanitize(decodedJwt.payload.lapValid);
	    response.locals.dateSet = request.sanitize(decodedJwt.payload.dateSet);

        return next();
    } catch (error) {
        return response.status(403).send(error);
    }
}

app.get("/getFilteredLaps", (request, response) => {
    const params = {};
    params.driverName = request.sanitize(request.query.driver);
    params.carName = request.sanitize(request.query.car);
    params.trackName = request.sanitize(request.query.track);
    params.className = request.sanitize(request.query.class);
    params.dateStart = request.sanitize(request.query.dateStart);
    params.dateEnd = request.sanitize(request.query.dateEnd);
    params.validOnly = request.sanitize(request.query.validOnly);
    params.bestOnly = request.sanitize(request.query.bestOnly);

    const query = createSelectFilteredLapsQuery(params);
    pool.query(query, (error, results) => {
        if (error) {
            response.status(404).send(error.message);
        } else {
            response.status(200).json(results.rows);
        }
    })
});

app.get("/getAllTrackNames", (request, response) => {
    const query = "SELECT DISTINCT id, pretty_name FROM Tracks ORDER BY pretty_name ASC";
    pool.query(query, (error, results) => {
        if (error) {
            response.status(404).send(error.message);
        } else {
            response.status(200).json(results.rows);
        }
    })
});

app.post("/addLap", JWTVerifier, (request, response, next) => {
    const driver = response.locals.driver;
    const carId = response.locals.carId;
    const carName = response.locals.carName;
    const carClass = response.locals.carClass;
    const carType = response.locals.carType;
    const trackId = response.locals.trackId;
    const trackName = response.locals.trackName;
    const lapTime = response.locals.lapTime;
    const lapValid = response.locals.lapValid;
    const dateSet = response.locals.dateSet;

    pool.query("CALL insert_lapTime($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)", [driver, carId, carName, carClass, carType, trackId, trackName, lapTime, lapValid, dateSet], (error, results) => {
        if (error) {
            response.status(404).send(error.message);
        } else {
            response.status(200).send();
        }
    });
});

function createParameters(params) {
    const cleanedParams = {};
    cleanedParams.driverName = params.driverName == null ? null : `'${params.driverName}'`;
    cleanedParams.carName = params.carName == null ? null : `'${params.carName}'`;
    cleanedParams.trackName = params.trackName == null ? null :`'${params.trackName}'`;
    cleanedParams.className = params.className == null ? null :`'${params.className}'`;
    cleanedParams.dateStart = params.dateStart == null ? `'0001-01-01T00:00:00Z'` : `'${params.dateStart}'`;
    cleanedParams.dateEnd = params.dateEnd == null ? `'9999-12-01T00:00:00Z'` : `'${params.dateEnd}'`;
    cleanedParams.validOnly = params.validOnly == null ? null : `'${params.validOnly}'`;
    cleanedParams.bestOnly = params.bestOnly == null ? null : `'${params.bestOnly}'`;

    return cleanedParams;
}

function createSelectFilteredLapsQuery(params) {
    const cleanedParams = createParameters(params);
    return `SELECT * FROM select_filteredLaps`
        + `(${cleanedParams.driverName}, ${cleanedParams.carName}, ${cleanedParams.trackName}, ${cleanedParams.className}, `
        + `${cleanedParams.dateStart}, ${cleanedParams.dateEnd}, ${cleanedParams.validOnly}, ${cleanedParams.bestOnly})`
}
