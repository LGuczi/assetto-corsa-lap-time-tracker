import { 
    FETCH_LAP_TIMES_REQUEST, 
    FETCH_LAP_TIMES_SUCCESS, 
    FETCH_LAP_TIMES_FAILURE,
    SORT_LAP_TIMES
} from '../actions/LapTimesAction';
import { combineReducers } from 'redux';

const getLapTimesInitialState = {
    isFetching: false,
    lapTimes: [],
    errorMessage: "",
    sortColumn: "Lap Time",
    sortAsc: true
}

function sortLapTimes(lapTimes, sortColumn, sortAscending) {
    switch(sortColumn) {
        case "Driver":
            return sortAscending === true 
                ? [].concat(lapTimes).sort((a, b) => a.driver > b.driver) 
                : [].concat(lapTimes).sort((a, b) => a.driver < b.driver);
        case "Track":
            return sortAscending === true 
                ? [].concat(lapTimes).sort((a, b) => a.track > b.track) 
                : [].concat(lapTimes).sort((a, b) => a.track < b.track);
        case "Car":
            return sortAscending === true 
                ? [].concat(lapTimes).sort((a, b) => a.car > b.car) 
                : [].concat(lapTimes).sort((a, b) => a.car < b.car);
        case "Class":
            return sortAscending === true 
                ? [].concat(lapTimes).sort((a, b) => a.carclass> b.carclass) 
                : [].concat(lapTimes).sort((a, b) => a.carclass < b.carclass);
        case "Type":
            return sortAscending === true 
                ? [].concat(lapTimes).sort((a, b) => a.cartype> b.cartype) 
                : [].concat(lapTimes).sort((a, b) => a.cartype < b.cartype);
        case "Lap Time":
            return sortAscending === true 
                ? [].concat(lapTimes).sort((a, b) => a.lap> b.lap) 
                : [].concat(lapTimes).sort((a, b) => a.lap < b.lap);
        case "Date/Time Set":
            return sortAscending === true 
                ? [].concat(lapTimes).sort((a, b) => a.datetimeset> b.datetimeset) 
                : [].concat(lapTimes).sort((a, b) => a.datetimeset < b.datetimeset);
        default:
            return lapTimes;            
    }
}

function handleGetLapTimeAction(state = getLapTimesInitialState, action) {
    switch (action.type) {
        case FETCH_LAP_TIMES_REQUEST:
            return {
                ...state,
                isFetching: true
            };
        case FETCH_LAP_TIMES_SUCCESS:
            return {
                ...state,
                isFetching: false,
                lapTimes: sortLapTimes(action.lapTimes, state.sortColumn, state.sortAsc),
            };
        case FETCH_LAP_TIMES_FAILURE:
            return {
                ...state,
                isFetching: false, 
                errorMessage: action.errorMessage
            };
        case SORT_LAP_TIMES:
            const sortAscending = state.sortColumn !== action.sortColumn ? true : !state.sortAsc;
            return {
                ...state,
                lapTimes: sortLapTimes(state.lapTimes, action.sortColumn, sortAscending),
                sortColumn: action.sortColumn,
                sortAsc: sortAscending
            };
        default:
            return state;
    }
}

const AppReducer = combineReducers({
    laps: handleGetLapTimeAction
})

export default AppReducer;
