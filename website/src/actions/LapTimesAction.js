import fetch from 'cross-fetch';

export const FETCH_LAP_TIMES_REQUEST = 'FETCH_LAP_TIMES_REQUEST';
export const FETCH_LAP_TIMES_SUCCESS = 'FETCH_LAP_TIMES_SUCCES';
export const FETCH_LAP_TIMES_FAILURE = 'FETCH_LAP_TIMES_FAILURE';
export const SORT_LAP_TIMES = 'SORT_LAP_TIMES';

export function getLapTimesRequest() {
    return {
        type: FETCH_LAP_TIMES_REQUEST
    };
}

export function getLapTimesSuccess(lapTimes) {
    return {
        type: FETCH_LAP_TIMES_SUCCESS,
        lapTimes: lapTimes
    }
}
  
export function getLapTimesFailure(errorMessage) {
    return {
        type: FETCH_LAP_TIMES_FAILURE,
        errorMessage: errorMessage
    };
}

function getUrlParams(filters) {
    let params = ''
    if (filters == null) {
        return params;
    }

    if (filters.driverFilter != null && filters.driverFilter.length > 0) {
        params += `&driver=${filters.driverFilter}`;
    }
    if (filters.trackFilter != null && filters.trackFilter.length > 0) {
        params += `&track=${filters.trackFilter}`;
    }
    if (filters.carFilter != null && filters.carFilter.length > 0) {
        params += `&car=${filters.carFilter}`;
    }
    if (filters.classFilter != null && filters.classFilter.length > 0) {
        params += `&class=${filters.classFilter}`;
    }
    if (filters.startDateFilter != null && filters.startDateFilter.length > 0) {
        params += `&dateStart=${filters.startDateFilter}`;
    }
    if (filters.endDateFilter != null && filters.endDateFilter.length > 0) {
        params += `&dateEnd=${filters.endDateFilter}`;
    }
    if (filters.bestLapsFilter != null) {
        params += `&bestOnly=${filters.bestLapsFilter}`;
    }
    if (filters.validLapsFilter != null) {
        params += `&validOnly=${filters.validLapsFilter}`;
    }

    return params;
}

export function fetchLapTimes(filters) {
    const params = getUrlParams(filters);

    return async dispatch => {
        dispatch(getLapTimesRequest());
        try {
            let response = await fetch(`http://99.250.30.224:3000/getFilteredLaps?${params}`);
            let json = await response.json();
            dispatch(getLapTimesSuccess(json));
        }
        catch(error) {
            dispatch(getLapTimesFailure(error));
        }
    }
}

  
export function sortLapTimes(column) {
    return {
        type: SORT_LAP_TIMES,
        sortColumn: column
    };
}
