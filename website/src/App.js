import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/App.css';

import LapTimesBody from './components/LapTimesBody'

function App() {
    return (
        <div className="App">
            <header className="App-header">
                Assetto Corsa Lap Times
            </header>
            <LapTimesBody />
        </div>
    );
}

export default App;
