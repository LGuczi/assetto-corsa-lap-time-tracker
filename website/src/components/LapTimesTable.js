import React from 'react';
import PropTypes from 'prop-types'; 
import Table from 'react-bootstrap/Table';
import Moment from 'moment';
import '../css/LapTimesTable.css';
import 'font-awesome/css/font-awesome.min.css';

const headers = {
    DRIVER: "Driver",
    TRACK: "Track",
    CAR: "Car",
    CAR_CLASS: "Class",
    CAR_TYPE: "Type",
    LAP_TIME: "Lap Time",
    DATE_SET: "Date/Time Set"
};

function formatDateTime(dateTime) {
    Moment.locale('en');
    return Moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
}

function formatLapTime(lapTime) {
    Moment.locale('en');
    return Moment(lapTime).format("mm:ss.SSS");
}

function createRows(item, key) {
    const validLap = item.validlap ? "Lap-time-valid" : "Lap-time-invalid";

    return (
        <tr key = {key}>
            <td>{item.driver}</td>
            <td>{item.track}</td>
            <td>{item.car}</td>
            <td>{item.carclass}</td>
            <td>{item.cartype}</td>
            <td className={validLap}>
                {formatLapTime(item.lap)}
            </td>
            <td>{formatDateTime(item.datetimeset)}</td>
        </tr>
    );
}

function drawSortArrow(targetColumn, currentColumn, sortAsc) {
    if (targetColumn === currentColumn) {
        const arrow = sortAsc === true ? "fa fa-arrow-down" : "fa fa-arrow-up"
        return (
            <i 
                class={arrow}
                style={{marginLeft: '5px'}}
            ></i>
        );
    } else {
        return;
    }
}

class LapTimesTable extends React.Component {
    createHeaders() {
        return (
            <tr>
                <th onClick={(event) => this.props.onSort(event.target.innerText)}>
                    {headers.DRIVER}
                    {drawSortArrow(this.props.sortColumn, headers.DRIVER, this.props.sortAsc)}
                </th>
                <th onClick={(event) => this.props.onSort(event.target.innerText)}>
                    {headers.TRACK}
                    {drawSortArrow(this.props.sortColumn, headers.TRACK, this.props.sortAsc)}
                </th>
                <th onClick={(event) => this.props.onSort(event.target.innerText)}>
                    {headers.CAR}
                    {drawSortArrow(this.props.sortColumn, headers.CAR, this.props.sortAsc)}
                </th>
                <th onClick={(event) => this.props.onSort(event.target.innerText)}>
                    {headers.CAR_CLASS}
                    {drawSortArrow(this.props.sortColumn, headers.CAR_CLASS, this.props.sortAsc)}
                </th>
                <th onClick={(event) => this.props.onSort(event.target.innerText)}>
                    {headers.CAR_TYPE}
                    {drawSortArrow(this.props.sortColumn, headers.CAR_TYPE, this.props.sortAsc)}
                </th>
                <th onClick={(event) => this.props.onSort(event.target.innerText)}>
                    {headers.LAP_TIME}
                    {drawSortArrow(this.props.sortColumn, headers.LAP_TIME, this.props.sortAsc)}
                </th>
                <th onClick={(event) => this.props.onSort(event.target.innerText)}>
                    {headers.DATE_SET}
                    {drawSortArrow(this.props.sortColumn, headers.DATE_SET, this.props.sortAsc)}
                </th>
            </tr>
        );
    }

    render() {
        return (
            <div className='Table-section'>
                <Table striped bordered hover size="sm">
                    <thead>
                        {this.createHeaders()}
                    </thead>
                    <tbody>
                        {this.props.lapTimes.map(createRows)}
                    </tbody>
                </Table>
            </div>
        )
    }
}

LapTimesTable.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    lapTimes: PropTypes.array.isRequired,
    onSort: PropTypes.func.isRequired,
    sortColumn: PropTypes.string.isRequired,
    sortAsc: PropTypes.bool.isRequired
};

export default LapTimesTable;