import React from 'react';
import { connect } from 'react-redux'
import LapTimesFilter from './LapTimesFilter';
import LapTimesTable from './LapTimesTable';
import { fetchLapTimes, sortLapTimes } from '../actions/LapTimesAction';

const UPDATE_INTERVAL_MS = 10000;

class LapTimesBody extends React.Component {
    componentDidMount() {
        this.props.getLapTimes();
    }

    render() {
        return (
            <div className="App-body">
                <div className="row">
                    <div className="Column-filters">
                        <LapTimesFilter 
                            onFilterSubmit={this.props.getLapTimes}
                            updateInterval={UPDATE_INTERVAL_MS}
                        />
                    </div>
                    <div className="Column-table">
                        <LapTimesTable
                            isFetching={this.props.isFetching}
                            lapTimes={this.props.lapTimes}
                            sortColumn={this.props.sortColumn}
                            sortAsc={this.props.sortAsc}
                            onSort={this.props.sortLapTimes}
                        />
                    </div>
                </div>
            </div>
        )        
    }
}

function mapStateToProps(state) {
    return {
        isFetching: state.laps.isFetching,
        lapTimes: state.laps.lapTimes,
        sortColumn: state.laps.sortColumn,
        sortAsc: state.laps.sortAsc
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getLapTimes: (filters) => dispatch(fetchLapTimes(filters)),
        sortLapTimes: (sortColumn) => dispatch(sortLapTimes(sortColumn))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LapTimesBody);
