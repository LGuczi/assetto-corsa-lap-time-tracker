import React from 'react';
import PropTypes from 'prop-types'; 
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { Col, ButtonToolbar } from 'react-bootstrap';
import '../css/LapTimesFilter.css';

class LapTimesFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            filters: {
                driverFilter: '',
                trackFilter: '',
                carFilter: '',
                classFilter: '',
                startDateFilter: '',
                endDateFilter: '',
                bestLapsFilter: '',
                validLapsFilter: ''
            }
        };
    }

    componentDidMount() {
        if (this.props.updateInterval > 0) {
            this.interval = setInterval(async () =>  {
                this.props.onFilterSubmit(this.state.filters);
            }, this.props.updateInterval);
        }
    }
    
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    submitFilters = event => {    
        event.preventDefault();
    
        const form = event.currentTarget;
        this.setState({
            filters: {
                driverFilter: form.driverFilter.value,
                trackFilter: form.trackFilter.value,
                carFilter: form.carFilter.value,
                classFilter: form.classFilter.value,
                startDateFilter: form.startDateFilter.value,
                endDateFilter: form.endDateFilter.value,
                bestLapsFilter: form.bestLapsFilter.checked,
                validLapsFilter: form.validLapsFilter.checked
            }
        },
            () => {
                this.props.onFilterSubmit(this.state.filters)
            }
        )
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.submitFilters}>
                    <Form.Group controlId="driverFilter">
                        <Form.Control type="text" placeholder="Driver" />
                    </Form.Group>

                    <Form.Group controlId="trackFilter">
                        <Form.Control type="text" placeholder="Track" />
                    </Form.Group>

                    <Form.Group controlId="carFilter">
                        <Form.Control type="text" placeholder="Car" />
                    </Form.Group>

                    <Form.Group controlId="classFilter">
                        <Form.Control type="text" placeholder="Class" />
                    </Form.Group>

                    <Form.Group controlId="startDateFilter">
                        <Form.Control type="datetime-local" placeholder="Start Date" />
                    </Form.Group>

                    <Form.Group controlId="endDateFilter">
                        <Form.Control type="text" placeholder="End Date" />
                    </Form.Group>

                    <Form.Row>
                        <Form.Group controlId="bestLapsFilter" as={Col}>
                            <Form.Check type="checkbox" label="Best Laps" />
                        </Form.Group>

                        <Form.Group controlId="validLapsFilter" as={Col}>
                            <Form.Check type="checkbox" label="Valid Laps" />
                        </Form.Group>
                    </Form.Row>
                    <ButtonToolbar>
                        <Button className="Button-submit" variant="primary" type="submit" >Submit</Button>
                        <Button className="Button-reset" variant="secondary" type="reset" >Clear</Button>
                    </ButtonToolbar>
                </Form>
            </div>
        )
    }
}

LapTimesFilter.propTypes = {
    onFilterSubmit: PropTypes.func.isRequired,
    updateInterval: PropTypes.number.isRequired
};

export default LapTimesFilter;