import React from 'react';

import '../css/App.css';

class LapTimesHeader extends React.Component {
    navigate(target) {
        console.log(target);
    }

    render() {
        return (
            <header className="App-header">
                <label>Assetto Corsa Lap Times</label>
                <div className="navigation-group">
                    <button
                        type="button"
                        onClick={() => this.navigate("LapTimes")}
                    >Lap Times
                    </button>
                    <button
                        onClick={() => this.navigate("Progress")}
                    >Progress
                    </button>
                </div>
            </header>
        )
    }
}

export default LapTimesHeader;